using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Telephony;
using Java.Net;
using System.Net;

namespace RemoteContentManager
{
    public class DeviceHelper
    {
        public static string GetIMEI(Activity activity)
        {
            var telephonyManager = (TelephonyManager)activity.GetSystemService("phone");
            return telephonyManager.DeviceId;
        }
        public static string GetIPAddress()
        {
            IPAddress[] addresses = Dns.GetHostAddresses(Dns.GetHostName());
            string ipAddress = string.Empty;
            if (addresses != null && addresses[0] != null)
            {
                ipAddress = addresses[0].ToString();
            }
            else
            {
                ipAddress = null;
            }
            return ipAddress;
        }
    }
}