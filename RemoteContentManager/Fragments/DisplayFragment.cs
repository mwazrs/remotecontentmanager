using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.IO;
using Java.Net;
using Android.Graphics.Drawables;
using Android.Graphics;
using System.IO;
using System.Security.Policy;
using System.Net;
using RemoteContentManager.Model;
using Android.Media;
using RemoteContentManager.Infrastructure;

namespace RemoteContentManager.Fragments
{
    public class DisplayFragment : Fragment, MediaPlayer.IOnCompletionListener
    {
        private Timer timer;
        private VideoView _videoView;
        private RcmPlaylist _displayedPlaylist;
        public RcmPlaylist DisplayedPlaylist
        {
            get { return _displayedPlaylist; }
            set
            {
                _displayedPlaylist = value;
                StartDisplaying(value);
            }
        }
        private LinearLayout _emptyContainer;
        public string DisplayPath { get; set; }
        private int contentIndex = 0;
        private ImageView _imageVIew;
        public event EventHandler FragmentLoaded = delegate { };
       // public List<Content> content;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //DisplayedPackage = new RcmPackage();
            DisplayPath = Android.OS.Environment.ExternalStorageDirectory + "/rcm_files";
            timer = new Timer();
            timer.Elapsed += Timer_Elapsed1;
        }

       

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.ImageDisplay, container, false);
            _imageVIew = view.FindViewById<ImageView>(Resource.Id.imageView1);
            _videoView = view.FindViewById<VideoView>(Resource.Id.videoView1);
            _emptyContainer = view.FindViewById<LinearLayout>(Resource.Id.emptyContainer);
            return view;
        }
        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            FragmentLoaded.Invoke(this, new EventArgs());
        }
        public void StartDisplaying(RcmPlaylist package)
        {
            if (timer != null) { timer.Stop(); }
            _emptyContainer.Visibility = ViewStates.Gone;
            var ext = FilesManager.GetContentType(package.Queue[contentIndex].ContentToDisplay.FileExtension);
            if (ext == ContentType.Image)
            {
                timer.Interval = package.Queue[contentIndex].ImageDisplayInterval * 1000;
                timer.Start();
                try
                {
                    var bitmap = BitmapFactory.DecodeFile(DisplayPath + "/" + package.Queue[contentIndex].ContentToDisplay.FileName + package.Queue[contentIndex].ContentToDisplay.FileExtension);
                    Activity.RunOnUiThread(() =>
                    {
                        _videoView.Visibility = ViewStates.Invisible;
                        _imageVIew.Visibility = ViewStates.Visible;
                        if (bitmap != null)
                        {
                            View.FindViewById<ImageView>(Resource.Id.imageView1).SetImageBitmap(bitmap);
                        }
                    });
                  
                  
                }catch(Exception ex)
                {

                    var x = ex;
                }
            
            }
            else
            {
                var path = DisplayPath + "/" + package.Queue[contentIndex].ContentToDisplay.FileName + package.Queue[contentIndex].ContentToDisplay.FileExtension;
                Activity.RunOnUiThread(() =>
                {
                    _imageVIew.Visibility = ViewStates.Invisible;
                    _videoView.Visibility = ViewStates.Visible;
                    var videoPlayer = new VideoPlayer(Activity, _videoView);
                    videoPlayer.SetCompletitionListener(this);
                    videoPlayer.PlayVideo(path);
                });
            }
            contentIndex++;
            if (contentIndex == package.Queue.Count)
            { contentIndex = 0; }
        }


        private void Timer_Elapsed1(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            StartDisplaying(DisplayedPlaylist);
        }
        public void OnCompletion(MediaPlayer mp)
        {
            try {
            StartDisplaying(DisplayedPlaylist);
            }catch(Exception ex)
            {
                var z = 23;
            }
        }
    }
}