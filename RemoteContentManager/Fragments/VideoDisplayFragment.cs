using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace RemoteContentManager.Fragments
{
    public class VideoDisplayFragment : Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.VideoDisplay, container, false);
        }
        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            var videoView = view.FindViewById<VideoView>(Resource.Id.videoView1);
            var mediaController = new MediaController(Activity);
            mediaController.SetAnchorView(videoView);
            videoView.SetMediaController(new MediaController(Activity));

            videoView.SetVideoPath(Android.OS.Environment.ExternalStorageDirectory + "/svideo.mp4");
            videoView.RequestFocus();
            videoView.Start();
        }
    }
}