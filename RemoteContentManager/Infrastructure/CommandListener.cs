using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace RemoteContentManager.Infrastructure
{
    class CommandListener
    {
        private TcpListener _listener;
        public event EventHandler<string> NewContentAvailable = delegate { };
        private bool ContinueListening = true;
        public event EventHandler<string> HostConnected = delegate { };
        public CommandListener(string ip, int port)
        {
            var localAdd = IPAddress.Parse(ip);
            _listener = new TcpListener(localAdd, port);
        }
        public void StartListening()
        {
            ContinueListening = true;
            ThreadPool.QueueUserWorkItem((s)=>
            {
                while (true)
                {
                    _listener.Start();
                    Console.WriteLine("Listening...");
                    var client = _listener.AcceptTcpClient();
                    var socket = client.Client;
                     var ip = "";
                    try { ip = socket.RemoteEndPoint.ToString().Split(':')[0]; } catch { }
                    if (!string.IsNullOrEmpty(ip)) { HostConnected(this, ip); }

                    var nwStream = client.GetStream();

                    byte[] buffer = new byte[client.ReceiveBufferSize];

                    int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                    string dataReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                    Console.WriteLine("Received : " + dataReceived);

                    if (dataReceived == "SEND_ARE_YOU_OKAY")
                    {
                        Console.WriteLine("Sending back : " + dataReceived);
                        byte[] bts = ASCIIEncoding.ASCII.GetBytes("STATUS_DEVICE_OK");
                        nwStream.Write(bts, 0, bts.Length);
                    }
                    else
                    {
                        NewContentAvailable(this, dataReceived);

                    }

                    client.Close();
                    _listener.Stop();
                    Console.WriteLine("Listener stopped...");
                    Thread.Sleep(200);
                }
            });
        }
        public void StopListening()
        {
            ContinueListening = false;
        }

    }
}