using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using RemoteContentManager.Model;

namespace RemoteContentManager.Infrastructure
{
    public class DataService
    {
        public static HttpClientEx RestClient
        {
            get { return TinyIoC.TinyIoCContainer.Current.Resolve<HttpClientEx>(); }
        }
        public static void RegisterHttpClient(string url)
        {
            TinyIoC.TinyIoCContainer.Current.Register(new HttpClientEx(url));
        }
        public async static Task<List<RcmPlaylist>> GetUrlsForDevie(string imei)
        {
            return await RestClient.GetAsyncEx<List<RcmPlaylist>>("Gallery/Download/" + imei);
        }
    }
}