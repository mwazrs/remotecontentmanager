using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Util;
using RemoteContentManager.Model;

namespace RemoteContentManager.Infrastructure
{
    public class DisplayTask : TimerTask
    {
        public event EventHandler<RcmPlaylist> OnScheduledTime = delegate { };
        public RcmPlaylist Content { get; set; }
        public DisplayTask(RcmPlaylist _content)
        {
            Content = _content;
        }

        public override void Run()
        {
            OnScheduledTime.Invoke(this, Content);
        }
    }
}