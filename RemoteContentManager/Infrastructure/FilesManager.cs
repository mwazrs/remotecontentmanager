using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RemoteContentManager.Model;
using Java.IO;
using System.Net;

namespace RemoteContentManager.Infrastructure
{
    public class FilesManager
    {
       
        public List<Content> DownloadedFiles { get; set; }
        public FilesManager()
        {
            DownloadedFiles = new List<Content>();
        }
        public void DownloadFiles(List<RcmPlaylist> _contentItems)
        {
            foreach (var item in _contentItems)
            {
                foreach(var file in item.Queue)
                {
                    var path = Android.OS.Environment.ExternalStorageDirectory + "/rcm_files/";
                    var fileExists = FileAlreadyExists(path + file.ContentToDisplay.FileName + file.ContentToDisplay.FileExtension);
                    if(!fileExists)
                    {
                        DownloadFIle(new Uri(WebContext.HostAdress +"Upload/"+ file.ContentToDisplay.FileName + file.ContentToDisplay.FileExtension), file.ContentToDisplay);
                    }
                }
            }
        }
        private bool FileAlreadyExists(string filePath)
        {
            var file = new File(filePath);
            return file.Exists();
        }
        private void DownloadFIle(Uri uri, RcmFile file)
        {

            try
            {
                var path = "";// Android.OS.Environment.ExternalStorageDirectory + "/rcm_files" + item.;
                path = Android.OS.Environment.ExternalStorageDirectory + "/rcm_files/"+file.FileName+file.FileExtension;

                using (var webClient = new WebClient())
                {
                    var imageBytes = webClient.DownloadData(uri);
                    var stream = new FileOutputStream(path);
                    stream.Write(imageBytes);
                }
               // DownloadedFiles.Add(new Content() {Path = path, ShowDateTime=null});

            }
            catch(Exception EX)
            {
                var x = EX;
            }


        }
        private void ClearDirectory(string path)
        {
            //  + "/rcm_files"
            var dir = new Java.IO.File(path);
            if (dir.IsDirectory)
            {
                string[] children = dir.List();
                for (int i = 0; i < children.Length; i++)
                {
                    new Java.IO.File(dir, children[i]).Delete();
                }
            }
        }
        public static ContentType GetContentType(string extension)
        {
            string[] imageTypes = {".jpg",".png"};
            string[] videoTypes = { ".mp4", ".mov", ".avi" };
            if (imageTypes.Contains(extension.ToLower()))
            {
                return ContentType.Image;
            }
            else if (videoTypes.Contains(extension.ToLower()))
            {
                return ContentType.Video;
            }
            else return ContentType.NotDefined;
           
        }
        //public static List<Content> GetContentFromDir(string dirPath)
        //{
        //    var f = new File(dirPath);
        //    var pathList = new List<Content>();
        //    var files = f.ListFiles();
        //    if (files.Length > 0)
        //    {
        //        foreach (var item in files)
        //        {
        //            if (item.Name.EndsWith(".mp4"))
        //            {
        //                pathList.Add(new Content() { Path = dirPath + "/" + item.Name, ContentType = ContentType.Video });
        //            }
        //            if (item.Name.EndsWith(".jpg") || item.Name.EndsWith(".png"))
        //            {
        //                pathList.Add(new Content() { Path = dirPath + "/" + item.Name, ContentType = ContentType.Image });
        //            }
        //        }
        //    }
        //    return pathList;
        //}
        public static int CountFilesInFolder(string folder)
        {
            var path = Android.OS.Environment.ExternalStorageDirectory.ToString() + "/" + folder;
            return (new File(path)).ListFiles().Length;
        }

        public static void CleanFilesDirectory(List<RcmPlaylist> playlists)
        {
            var path = Android.OS.Environment.ExternalStorageDirectory + "/rcm_files";
            var dir = new File(path);
            var files = dir.ListFiles();

            foreach(var playlist in playlists)
            {
                foreach(var intFile in files)
                {
                    var file = playlist.Queue.Where(x => (x.ContentToDisplay.FileName+ x.ContentToDisplay.FileExtension).Equals(intFile.Name));
                    if(file.Count()==0)
                    {
                        intFile.Delete();
                    }
                }
               
            }
        }
     
     
    }
}