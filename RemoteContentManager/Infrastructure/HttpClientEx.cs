using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using Newtonsoft.Json.Converters;

namespace RemoteContentManager.Infrastructure
{
    public class HttpClientEx
    {
        private HttpClient _httpClient;
        public HttpClientEx(string url)
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(url);
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public async Task<TResult> GetAsyncEx<TResult>(string uri)
        {
            var response = await _httpClient.GetAsync(uri);
            response.EnsureSuccessStatusCode();
            var contentString = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TResult>(contentString, 
            new JsonSerializerSettings
            {
                 Converters = { new IsoDateTimeConverter() { DateTimeFormat= "yyyy-MM-ddTHH:mm:ss" } },

            }
            );

        }
    }
}