using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace RemoteContentManager.Infrastructure
{
    public class PreferencesStorage
    {
        private ISharedPreferences prefs;
        private ISharedPreferencesEditor prefEditor;
        public PreferencesStorage()
        {
            prefs = Application.Context.GetSharedPreferences("MyApp", FileCreationMode.Private);
            prefEditor = prefs.Edit();
            prefEditor.PutString("PrefName", "Some value");
            prefEditor.Commit();
        }
        public void SaveValue<T>(T val, string key)
        {
            prefEditor = prefs.Edit();
            var str = JsonConvert.SerializeObject(val);
            prefEditor.PutString(key, str);
            prefEditor.Commit();
        }
        public T GetValue<T>(string key)
        {
            var str = prefs.GetString(key, "");
            return JsonConvert.DeserializeObject<T>(str);
        }
    }
    public class StoreKeys
    {
        public static string ScheduledTask = "scheduledTask";
    }
}