using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Util;
using RemoteContentManager.Model;

namespace RemoteContentManager.Infrastructure
{
    public class ScheduleManager
    {
        private Timer timer;
        //s public List<DisplayTask> Tasks { get; set; }
        public event EventHandler<RcmPlaylist> ScheduledContentToDisplay = delegate { };
        private List<RcmPlaylist> _content;
        private Activity context;
       
        public List<RcmPlaylist> Content {
            get { return _content; }
            set { _content = value; }
        }
        public ScheduleManager(Activity _context)
        {
            timer = new Timer();
            context = _context;
            _content = new List<RcmPlaylist>();
            
        }
        public void ScheduleTasks()
        {

            foreach (var item in _content)
            {
                if (item.DisplayDateTime != null)
                {
                    var task = new DisplayTask(item);
                    task.OnScheduledTime += Task_OnScheduledTime;
                    timer.Schedule(task, GetDate(item.DisplayDateTime));
                }
            }
            _content = new List<RcmPlaylist>();
        }

        private void Task_OnScheduledTime(object sender, RcmPlaylist e)
        {
            context.RunOnUiThread(() =>
            {
                ScheduledContentToDisplay.Invoke(this, e);
            });
        }
        public void LoadAlreadyScheuled()
        {
            var toLoad = GetTaskToSchedule();
            if (toLoad != null)
            {
                if (toLoad.Count > 0)
                {
                    foreach (var item in toLoad)
                    {
                        _content.Add(item);
                    }
                }
            }
            
            ScheduleTasks();
        }
        private List<RcmPlaylist> GetTaskToSchedule()
        {
           var storage = new PreferencesStorage();
           return storage.GetValue<List<RcmPlaylist>>(StoreKeys.ScheduledTask);
        }

        private Date GetDate(DateTime? date)
        {
            var d = date.GetValueOrDefault().ToString("MM.dd.yyyy HH:mm");
            var format = new Java.Text.SimpleDateFormat("MM.dd.yyyy HH:mm"); 
            return format.Parse(d);
        }

    }
}