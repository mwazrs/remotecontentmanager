using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net.Sockets;
using RemoteContentManager.Model;
using System.Net;
using System.Threading.Tasks;

namespace RemoteContentManager.Infrastructure
{
    public class TcpClientEx
    {
        private TcpClientConfig _configuration;
        private TcpListener _listener;
        private string _data;
        public string Data
        {
            get { return _data; }
            private set
            {
                _data = value;
                if(DataReceived != null)
                {
                    DataReceived.Invoke(this, value);
                }
            }
        }
        public event EventHandler<string> DataReceived;
        public TcpClientEx(TcpClientConfig cfg)
        {
            _configuration = cfg;
            _listener = new TcpListener(IPAddress.Parse(_configuration.IpAddress), cfg.PortNumber);
        }
    }
}