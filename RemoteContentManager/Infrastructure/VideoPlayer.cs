using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;

namespace RemoteContentManager.Infrastructure
{
    public class VideoPlayer
    {
        // private Activity _activity;
        private VideoView _videoView;
        //public VideoView VideoView
        //{
        //    get { return _videoView; }
        //    set { _videoView = value; }
        //}
       // private List<string> _videoPaths;
      //  private int _counter = 0;
        //private string _contentDirectory;
        //public string ContentDirectory
        //{
        //    get { return _contentDirectory; }
        //    set
        //    {
        //        _contentDirectory = value;
        //     //   _videoPaths = GetVideoFilesFromDir(value);
        //    }
        //}

        public VideoPlayer(Activity activity, VideoView vv)
        {
            //_activity = activity;
            _videoView = vv;
            _videoView.SetMediaController(new MediaController(activity));
           
        }
        public void SetCompletitionListener(MediaPlayer.IOnCompletionListener listener)
        {
            _videoView.SetOnCompletionListener(listener);
        }
        public void PlayVideo(string path)
        {
            //var p = _videoPaths[_counter];
            _videoView.SetVideoPath(path);
            _videoView.Start();
        }
        //public void PlayNext()
        //{
        //    _counter++;
        //    if (_counter == _videoPaths.Count)
        //    {
        //        _counter = 0;
        //    }

        //    VideoView.SetVideoPath(_videoPaths[_counter]);
        //    VideoView.Start();
        //}
        //public void OnCompletion(MediaPlayer mp)
        //{
        //    PlayNext();
        //}



    }
}