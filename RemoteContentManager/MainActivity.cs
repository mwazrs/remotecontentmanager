﻿using Android.App;
using Android.Widget;
using Android.OS;
using RemoteContentManager.Infrastructure;
using RemoteContentManager.Model;
using Android.Net.Wifi;
using Android.Text.Format;
using Java.Math;
using RemoteContentManager.Fragments;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;
using System.Collections.Generic;
using Android.Telephony;
using Android.Content;
using Java.Util;
using System.Linq;
using Java.IO;

namespace RemoteContentManager
{
    [Activity(Label = "RemoteContentManager", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation =Android.Content.PM.ScreenOrientation.Landscape)]
    public class MainActivity : Activity
    {
        const int PORT_NO = 5000;
        private bool _hostRegistered = false;
        private DisplayFragment _fragment;
        private ScheduleManager _scheduleManager;
        FilesManager _fileManager;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            _scheduleManager = new ScheduleManager(this);
            _scheduleManager.ScheduledContentToDisplay += ScheduledPackageToDisplay;
            _fileManager = new FilesManager();
            _scheduleManager.LoadAlreadyScheuled();
            SetContentView (Resource.Layout.Listener);
            InitCmdListener();        
            StartFragment(); 
        }

        private void ScheduledPackageToDisplay(object sender, RcmPlaylist e)
        {
            _fragment.DisplayedPlaylist = e;
        }

        private void InitCmdListener()
        {
            var cmdListener = new CommandListener(DeviceHelper.GetIPAddress(), PORT_NO);

            cmdListener.HostConnected += CmdListener_HostConnected;
            cmdListener.NewContentAvailable += CmdListener_NewContentAvailable;
            cmdListener.StartListening();

        }

        private void CmdListener_HostConnected(object sender, string e)
        {
            if(!_hostRegistered)
            {
                var url = "http://" + e + "/wwwContentManager/";
                DataService.RegisterHttpClient(url);
                WebContext.HostAdress = url;
                _hostRegistered = true;
            }
          
        }

        private async void CmdListener_NewContentAvailable(object sender, string e)
        {
           // var cmd = e;
            var imei = DeviceHelper.GetIMEI(this);
            var list = await DataService.GetUrlsForDevie(imei);
            _fileManager.DownloadFiles(list);
     
            var store = new PreferencesStorage();
            store.SaveValue<List<RcmPlaylist>>(list, StoreKeys.ScheduledTask);
            _scheduleManager.Content = list;
            _scheduleManager.ScheduleTasks();
            FilesManager.CleanFilesDirectory(list);



        }
        private void StartFragment()
        {
            var cm = new ContentManager();
            var path = Android.OS.Environment.ExternalStorageDirectory.ToString();

            var fragmentTx = FragmentManager.BeginTransaction();
            _fragment = new DisplayFragment();
            _fragment.FragmentLoaded += _fragment_FragmentLoaded;
            fragmentTx.Add(Resource.Id.displayContainer, _fragment);
            fragmentTx.AddToBackStack(null);
            fragmentTx.Commit();
            //_fragment.RefreshScreen();
        }

        private void _fragment_FragmentLoaded(object sender, EventArgs e)
        {
            var cont = Android.OS.Environment.ExternalStorageDirectory + "/rcm_files/image1.jpg";
            //_fragment.SetContentToDisplay(cont, ContentType.Image);
            //_fragment.StartDisplaying();
        }
    }
}

