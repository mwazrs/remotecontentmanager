using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RemoteContentManager.Infrastructure;

namespace RemoteContentManager.Model
{
    public class Content
    {
        //public ContentType ContentType { get; set; }
        public string Path { get; set; }
        public DateTime? ShowDateTime { get; set; }
   
    }
}