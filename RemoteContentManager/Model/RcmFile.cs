using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RemoteContentManager.Infrastructure;

namespace RemoteContentManager.Model
{
    public class RcmFile
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string ServerIp { get; set; }
        public string LocalPath { get; set; }

    }
}