using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RemoteContentManager.Model
{
    public class RcmPlaylist
    {
        public int Id { get; set; }
        public virtual List<RcmPackage> Queue { get; set; }
        
        public DateTime DisplayDateTime { get; set; }
        
    }
}