#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Android.Runtime.ResourceDesignerAttribute("RemoteContentManager.Resource", IsApplication=true)]

namespace RemoteContentManager
{
	
	
	[System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Android.Build.Tasks", "1.0.0.0")]
	public partial class Resource
	{
		
		static Resource()
		{
			global::Android.Runtime.ResourceIdManager.UpdateIdValues();
		}
		
		public static void UpdateIdValues()
		{
		}
		
		public partial class Attribute
		{
			
			static Attribute()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Attribute()
			{
			}
		}
		
		public partial class Drawable
		{
			
			// aapt resource value: 0x7f020000
			public const int empty = 2130837504;
			
			// aapt resource value: 0x7f020001
			public const int Icon = 2130837505;
			
			// aapt resource value: 0x7f020002
			public const int image1 = 2130837506;
			
			// aapt resource value: 0x7f020003
			public const int image2 = 2130837507;
			
			// aapt resource value: 0x7f020004
			public const int image3 = 2130837508;
			
			static Drawable()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Drawable()
			{
			}
		}
		
		public partial class Id
		{
			
			// aapt resource value: 0x7f060005
			public const int displayContainer = 2131099653;
			
			// aapt resource value: 0x7f060004
			public const int emptyContainer = 2131099652;
			
			// aapt resource value: 0x7f060002
			public const int frameLayout1 = 2131099650;
			
			// aapt resource value: 0x7f060000
			public const int imageView1 = 2131099648;
			
			// aapt resource value: 0x7f060001
			public const int textView1 = 2131099649;
			
			// aapt resource value: 0x7f060003
			public const int videoView1 = 2131099651;
			
			static Id()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Id()
			{
			}
		}
		
		public partial class Layout
		{
			
			// aapt resource value: 0x7f030000
			public const int Empty = 2130903040;
			
			// aapt resource value: 0x7f030001
			public const int ImageDisplay = 2130903041;
			
			// aapt resource value: 0x7f030002
			public const int Listener = 2130903042;
			
			// aapt resource value: 0x7f030003
			public const int Main = 2130903043;
			
			// aapt resource value: 0x7f030004
			public const int VideoDisplay = 2130903044;
			
			static Layout()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Layout()
			{
			}
		}
		
		public partial class String
		{
			
			// aapt resource value: 0x7f040001
			public const int ApplicationName = 2130968577;
			
			// aapt resource value: 0x7f040000
			public const int Hello = 2130968576;
			
			static String()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private String()
			{
			}
		}
		
		public partial class Style
		{
			
			// aapt resource value: 0x7f050000
			public const int AppTheme = 2131034112;
			
			// aapt resource value: 0x7f050001
			public const int NoActionBar = 2131034113;
			
			static Style()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Style()
			{
			}
		}
	}
}
#pragma warning restore 1591
