﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TcpSender
{
    class Program
    {
        //te same dane wprowadzic w telefonie
        const int PORT_NO = 5000;
        const string SERVER_IP = "127.0.0.1";


        static void Main(string[] args)
        {
            IPAddress localAdd = IPAddress.Parse(SERVER_IP);
            TcpListener listener = new TcpListener(localAdd, PORT_NO);
            
            
            while (true)
            {
                listener.Start();
                Console.WriteLine("Listening...");
                var client = listener.AcceptTcpClient();
                var nwStream = client.GetStream();

                byte[] buffer = new byte[client.ReceiveBufferSize];

                int bytesRead = nwStream.Read(buffer, 0, client.ReceiveBufferSize);

                string dataReceived = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                Console.WriteLine("Received : " + dataReceived);

                Console.WriteLine("Sending back : " + dataReceived);
                nwStream.Write(buffer, 0, bytesRead);
                client.Close();
                listener.Stop();
                Console.WriteLine("Listener stopped...");
                Thread.Sleep(200);
            }
            

            //Console.ReadLine();
        }
   
    }
}
