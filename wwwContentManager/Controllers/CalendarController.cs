﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using wwwContentManager.Models;

namespace wwwContentManager.Controllers
{
    public class CalendarController : Controller
    {
        // GET: Calendar
        ApplicationDbContext Db = new ApplicationDbContext();

        public ActionResult Index()
        {
            var playlists = Db.Playlists.ToList();
            return View(playlists);
        }
    }
}