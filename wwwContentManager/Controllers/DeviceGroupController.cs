﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using wwwContentManager.Models;
using wwwContentManager.Services;

namespace wwwContentManager.Controllers
{
    public class DeviceGroupController : Controller
    {
        ApplicationDbContext Db = new ApplicationDbContext();

        public ActionResult Index()
        {
            return View(Db.DeviceGroups.ToList());
        }

        public ActionResult Create()
        {
            var model = new CreateDeviceGroupViewModel();
            var devices = Db.Devices.ToList();
            model.DeviceGroup = new DeviceGroup();
            model.Devices = new List<DeviceViewModel>();
            for (int i = 0; i < devices.Count; i++)
            {
                var item = new DeviceViewModel();
                item.Selected = false;
                item.Name = devices[i].Name;
                item.Id = devices[i].Id;
                model.Devices.Add(item);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CreateDeviceGroupViewModel group)
        {
            var newGroup = new DeviceGroup();
            newGroup.Name = group.DeviceGroup.Name;
            foreach (var device in group.Devices)
            {
                if (device.Selected)
                {
                    var item = Db.Devices.Find(device.Id);
                    newGroup.Devices.Add(item);
                    item.DeviceGroups.Add(newGroup);
                    Db.Entry(item).State = EntityState.Modified;
                }
            }
            Db.DeviceGroups.Add(newGroup);
            Db.Entry(newGroup).State = EntityState.Added;
            Db.SaveChanges();
            return Redirect("Index");
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceGroup group = Db.DeviceGroups.Find(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return View(group);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeviceGroup device = Db.DeviceGroups.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        [HttpPost]
        public ActionResult Edit(DeviceGroup group)
        {
            if (ModelState.IsValid)
            {
                Db.Entry(group).State = EntityState.Modified;
                Db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(group);
        }

        public ActionResult Delete(int id)
        {
            DeviceGroup device = Db.DeviceGroups.Find(id);
            Db.DeviceGroups.Remove(device);
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult SendContent(int id)
        {
            var files = Db.Files.ToList();
            var isSelected = new List<bool>();
            var intervals = new List<int>();
            foreach (var file in files)
            {
                isSelected.Add(false);
                intervals.Add(10);
            }

            var viewModel = new GroupSendContentViewModel()
            {
                Files = files,
                IsSelected = isSelected,
                GroupId = id,
                Date = DateTime.Now,
                DateEnd = DateTime.Now,
                ImageDisplayInterval = intervals
            };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SendContent(GroupSendContentViewModel model)
        {
            var files = Db.Files.ToList();
            var devices = Db.DeviceGroups.Include(x => x.Devices).Where(x => x.Id == model.GroupId).FirstOrDefault().Devices.ToList();
            var playlists = new List<Playlist>();
            for (int i = 0; i < devices.Count; i++)
            {
                var playlist = new Playlist() { DisplayDateTime = model.Date, EndDateTime = model.DateEnd, DeviceId = devices[i].Id };
                playlist.Queue = new List<Package>();
                Boolean sendingContent = false;
                Functions func = new Functions();
                var contentToDisplay = new List<File>();
                for (int j = 0; j < model.IsSelected.Count; j++)
                {
                    if (model.IsSelected[j])
                    {
                        var pack = new Package() { ImageDisplayInterval = model.ImageDisplayInterval[j], ContentToDisplay = files[j] };
                        playlist.Queue.Add(pack);
                        Db.Packages.Add(pack);
                        Db.Entry(pack).State = EntityState.Added;
                    }
                }

                Db.Playlists.Add(playlist);
                Db.Entry(devices[i]).State = EntityState.Modified;
                Db.SaveChanges();
                new Task(() =>
                {
                    sendingContent = func.sendCommand(deviceCommands.SEND_NEW_CONTENT, devices[i].Ip);
                }).Start();
            }
            return RedirectToAction("Index");
        }
    }
}