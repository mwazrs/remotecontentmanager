﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using wwwContentManager.Models;
using wwwContentManager.Services;

namespace wwwContentManager.Controllers
{
    public class DevicesController : Controller
    {
        ApplicationDbContext Db = new ApplicationDbContext();
        Functions FunctionsObj = new Functions();

        public ActionResult Index()
        {
            var devices = Db.Devices.ToList();
            Functions func = new Functions();
            var statuses = FunctionsObj.pingDevices(devices);
            for (int i = 0; i < devices.Count; i++)
            {
                devices[i].Synchronized = statuses[i];
            }
            return View(devices);
        }

        public ActionResult PingTest()
        {
            Functions func = new Functions();
            List<Device> deviceList = Db.Devices.ToList();
            List<Boolean> statuses = func.pingDevices(deviceList);
            return this.Json(statuses, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendCommandTest()
        {
            Functions func = new Functions();
            func.sendCommand(deviceCommands.SEND_ARE_YOU_OKAY, "192.168.120.101");
            return View();
        }

        public ActionResult SendMessage(string message)
        {

            try
            {
                int portNumber = 5000;
                string serverIP = "192.168.1.7";
                var tcpClient = new TcpClient(serverIP, portNumber);
                var newStream = tcpClient.GetStream();
                var bytesToSend = ASCIIEncoding.ASCII.GetBytes(message);
                newStream.Write(bytesToSend, 0, bytesToSend.Length);
                tcpClient.Close();
            }
            catch { }
            return View("index");
            //Tu trzeba połączyć się z urządzeniem i wysłać mu wiadomosc
        }

        public bool CheckStatus(int id)
        {
            //pingiem sprawdzić czy odpowiada. Jeśli tak to true, inaczej false
            return true;
        }

        //CRUD
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Device machine)
        {
            machine.Synchronized = true;
            Db.Devices.Add(machine);
            Db.SaveChanges();
            return Redirect("Index");
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device machine = Db.Devices.Find(id);
            if (machine == null)
            {
                return HttpNotFound();
            }
            return View(machine);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Device device = Db.Devices.Find(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(device);
        }

        [HttpPost]
        public ActionResult Edit(Device machine)
        {
            if (ModelState.IsValid)
            {
                Db.Entry(machine).State = EntityState.Modified;
                Db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(machine);
        }

        public ActionResult Delete(int id)
        {
            Device device = Db.Devices.Find(id);
            Db.Devices.Remove(device);
            Db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult ReadDevice(int id)
        {
            return View();
        }

        public ActionResult SendContent(int id)
        {
            var files = Db.Files.ToList();
            var isSelected = new List<bool>();
            var intervals = new List<int>();
            foreach (var file in files)
            {
                isSelected.Add(false);
                intervals.Add(10);
            }

            var viewModel = new DeviceSendContentViewModel()
            {
                Files = files,
                IsSelected =isSelected,
                DeviceId = id,
                Date = DateTime.Now,
                DateEnd = DateTime.Now,
                ImageDisplayInterval = intervals
            };
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SendContent(DeviceSendContentViewModel model)
        {
            var files = Db.Files.ToList();
            var device = Db.Devices.Find(model.DeviceId);
            var playlist = new Playlist() { DisplayDateTime = model.Date, EndDateTime = model.DateEnd, DeviceId = device.Id };
            playlist.Queue = new List<Package>();
            Boolean sendingContent = false;
            Functions func = new Functions();
            var contentToDisplay = new List<File>();
            for (int i = 0; i < model.IsSelected.Count; i++)
            {
                if (model.IsSelected[i])
                {
                    var pack = new Package() { ImageDisplayInterval = model.ImageDisplayInterval[i], ContentToDisplay = files[i] };
                    playlist.Queue.Add(pack);
                    Db.Packages.Add(pack);
                    Db.Entry(pack).State = EntityState.Added;
                }
            }

            Db.Playlists.Add(playlist); 
            Db.Entry(device).State = EntityState.Modified;
            Db.SaveChanges();
            new Task(() => {
                sendingContent = func.sendCommand(deviceCommands.SEND_NEW_CONTENT, device.Ip);
            }).Start();
            return RedirectToAction("Index");
        }
    }
}