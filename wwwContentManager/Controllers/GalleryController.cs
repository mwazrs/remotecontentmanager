﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using wwwContentManager.Models;
using Newtonsoft.Json;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System;

namespace wwwContentManager.Controllers
{
    public class GalleryController : Controller
    {
        ApplicationDbContext Db = new ApplicationDbContext();

        public ActionResult Index()
        {
            return View(Db.Files.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Upload/"), fileName);
                file.SaveAs(path);
                var workItem = new Models.File();
                workItem.FileName = Path.GetFileNameWithoutExtension(fileName);
                workItem.FileExtension = Path.GetExtension(fileName);
                Db.Files.Add(workItem);
                Db.SaveChanges();
            }
            return Redirect("Index");
        }

        public ActionResult DonloadById(int id)
        {
            var file = Db.Files.Find(id);
            Response.ContentType = "image/jpeg";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + file.FileName + file.FileExtension);
            Response.TransmitFile(Server.MapPath("~/Upload/" + file.FileName+file.FileExtension));
            Response.End();
            return RedirectToAction("");
        }

        public string Download(string id)
        {
            var device = Db.Devices.Where(d => d.IMEI == id).FirstOrDefault();
            var playlists = Db.Playlists.Where(x => x.DeviceId == device.Id && x.DisplayDateTime >= DateTime.Now);

            var host = GetIPAddress();
            var downloadPlaylists = new List<DownloadPlaylistViewModel>();
            if (playlists != null)
            {
                foreach (var playlist in playlists)
                {
                    var downloadPlaylist = new DownloadPlaylistViewModel()
                    {
                        Id = playlist.Id,
                        DisplayDateTime = playlist.DisplayDateTime,
                        EndDateTime = playlist.EndDateTime,
                        Queue = new List<DownloadPackageViewModel>()
                    };
                    for (int i = 0; i < playlist.Queue.Count; i++)
                    {
                        var downloadFile = new DownloadFileViewModel()
                        {
                            Id = playlist.Queue.ToList()[i].ContentToDisplay.Id,
                            FileName = playlist.Queue.ToList()[i].ContentToDisplay.FileName,
                            FileExtension = playlist.Queue.ToList()[i].ContentToDisplay.FileExtension,
                            ServerIp = host
                        };
                        var downloadPackage = new DownloadPackageViewModel()
                        {
                            Id = playlist.Queue.ToList()[i].Id,
                            ImageDisplayInterval = playlist.Queue.ToList()[i].ImageDisplayInterval,
                            ContentToDisplay = downloadFile
                        };
                        downloadPlaylist.Queue.Add(downloadPackage);
                    }
                    downloadPlaylists.Add(downloadPlaylist);
                }
                return JsonConvert.SerializeObject(downloadPlaylists);
            }    
            return "";
        }

        protected string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }
    }
}