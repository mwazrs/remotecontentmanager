namespace wwwContentManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DeviceGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileName = c.String(),
                        FileExtension = c.String(),
                        DeviceGroup_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DeviceGroups", t => t.DeviceGroup_Id)
                .Index(t => t.DeviceGroup_Id);
            
            CreateTable(
                "dbo.Devices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IMEI = c.String(),
                        Name = c.String(),
                        Description = c.String(),
                        Type = c.String(),
                        Ip = c.String(),
                        Port = c.Int(nullable: false),
                        Synchronized = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Packages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImageDisplayInterval = c.Int(nullable: false),
                        ContentToDisplay_Id = c.Int(),
                        Device_Id = c.Int(),
                        Playlist_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.ContentToDisplay_Id)
                .ForeignKey("dbo.Devices", t => t.Device_Id)
                .ForeignKey("dbo.Playlists", t => t.Playlist_Id)
                .Index(t => t.ContentToDisplay_Id)
                .Index(t => t.Device_Id)
                .Index(t => t.Playlist_Id);
            
            CreateTable(
                "dbo.Playlists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisplayDateTime = c.DateTime(nullable: false),
                        DeviceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Devices", t => t.DeviceId, cascadeDelete: true)
                .Index(t => t.DeviceId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.DeviceDeviceGroups",
                c => new
                    {
                        Device_Id = c.Int(nullable: false),
                        DeviceGroup_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Device_Id, t.DeviceGroup_Id })
                .ForeignKey("dbo.Devices", t => t.Device_Id, cascadeDelete: true)
                .ForeignKey("dbo.DeviceGroups", t => t.DeviceGroup_Id, cascadeDelete: true)
                .Index(t => t.Device_Id)
                .Index(t => t.DeviceGroup_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Packages", "Playlist_Id", "dbo.Playlists");
            DropForeignKey("dbo.Playlists", "DeviceId", "dbo.Devices");
            DropForeignKey("dbo.Packages", "Device_Id", "dbo.Devices");
            DropForeignKey("dbo.Packages", "ContentToDisplay_Id", "dbo.Files");
            DropForeignKey("dbo.DeviceDeviceGroups", "DeviceGroup_Id", "dbo.DeviceGroups");
            DropForeignKey("dbo.DeviceDeviceGroups", "Device_Id", "dbo.Devices");
            DropForeignKey("dbo.Files", "DeviceGroup_Id", "dbo.DeviceGroups");
            DropIndex("dbo.DeviceDeviceGroups", new[] { "DeviceGroup_Id" });
            DropIndex("dbo.DeviceDeviceGroups", new[] { "Device_Id" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Playlists", new[] { "DeviceId" });
            DropIndex("dbo.Packages", new[] { "Playlist_Id" });
            DropIndex("dbo.Packages", new[] { "Device_Id" });
            DropIndex("dbo.Packages", new[] { "ContentToDisplay_Id" });
            DropIndex("dbo.Files", new[] { "DeviceGroup_Id" });
            DropTable("dbo.DeviceDeviceGroups");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Playlists");
            DropTable("dbo.Packages");
            DropTable("dbo.Devices");
            DropTable("dbo.Files");
            DropTable("dbo.DeviceGroups");
        }
    }
}
