namespace wwwContentManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EndTimeMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Playlists", "EndDateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Playlists", "EndDateTime");
        }
    }
}
