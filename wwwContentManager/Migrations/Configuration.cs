namespace wwwContentManager.Migrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<wwwContentManager.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(wwwContentManager.Models.ApplicationDbContext context)
        {
            var device1 = new Device() { Ip="10.188.109.193", IMEI="356489057398146", Name="Zyzdrian", Description="Telefon Zyzdriana", Port=80, Type="Telefon"};
            var device2 = new Device() { Ip = "10.190.136.19", IMEI = "867576027402164", Name = "Hohel", Description = "Telefon Hohla", Port = 80, Type = "Telefon" };
            var devices = new List<Device>();
            devices.Add(device1);
            devices.Add(device2);

            context.Devices.AddOrUpdate(d => d.IMEI, devices.ToArray());
            context.SaveChanges();
        }
    }
}
