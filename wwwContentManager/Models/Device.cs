﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wwwContentManager.Models
{
    public class Device
    {
        public int Id { get; set; }
        public string IMEI { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Ip { get; set; }
        public int Port { get; set; }
        public bool Synchronized { get; set; }

        public virtual ICollection<Package> Packages { get; set; }
        public virtual ICollection<DeviceGroup> DeviceGroups { get; set; }
    }
}