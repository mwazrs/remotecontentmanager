﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wwwContentManager.Models
{
    public class DeviceGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Device> Devices { get; set; }
        public virtual ICollection<File> Contents { get; set; }
    }
}