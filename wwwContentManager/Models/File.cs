﻿namespace wwwContentManager.Models
{
    public class File
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
       
    }
}