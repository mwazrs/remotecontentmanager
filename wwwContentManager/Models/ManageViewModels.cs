﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Web;
using System;

namespace wwwContentManager.Models
{
    public class IndexViewModel
    {
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }

    public class DeviceViewModel
    {
        public int Id { get; set; }
        public string IMEI { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Ip { get; set; }
        public int Port { get; set; }
        public bool Selected { get; set; }
    }

    public class CreateDeviceGroupViewModel
    {
        public DeviceGroup DeviceGroup { get; set; }
        public ICollection<DeviceViewModel> Devices { get; set; }
    }

    public class FileViewModel
    {
        public HttpPostedFileBase File { get; set; }
    }

    public class DeviceSendContentViewModel
    {
        public List<File> Files { get; set; }
        public List<bool> IsSelected { get; set; }
        public DateTime Date { get; set; }
        public DateTime DateEnd { get; set; }
        public int DeviceId { get; set; }
        public List<int> ImageDisplayInterval { get; set; }
    }

    public class GroupSendContentViewModel
    {
        public List<File> Files { get; set; }
        public List<bool> IsSelected { get; set; }
        public DateTime Date { get; set; }
        public DateTime DateEnd { get; set; }
        public int GroupId { get; set; }
        public List<int> ImageDisplayInterval { get; set; }
    }

    public class DownloadPlaylistViewModel
    {
        public int Id { get; set; }
        public virtual ICollection<DownloadPackageViewModel> Queue { get; set; }
        public DateTime DisplayDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }

    public class DownloadPackageViewModel
    {
        public int Id { get; set; }
        public DownloadFileViewModel ContentToDisplay { get; set; }
        public int ImageDisplayInterval { get; set; }
    }

    public class DownloadFileViewModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string ServerIp { get; set; }
    }
}