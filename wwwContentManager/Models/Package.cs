﻿namespace wwwContentManager.Models
{
    public class Package
    {
        public int Id { get; set; }
        public virtual File ContentToDisplay { get; set; }
        public int ImageDisplayInterval { get; set; }
    }
}