﻿using System;
using System.Collections.Generic;

namespace wwwContentManager.Models
{
    public class Playlist
    {
        public int  Id { get; set; }
        public virtual ICollection<Package> Queue { get; set; }
        public DateTime DisplayDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int DeviceId { get; set; }
        public virtual Device Device { get; set; }
    }
}