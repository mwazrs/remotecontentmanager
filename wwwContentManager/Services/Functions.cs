﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Web;
using wwwContentManager.Models;

namespace wwwContentManager.Services
{
    public enum deviceCommands
    {
        SEND_NEW_CONTENT = 0,
        SEND_ARE_YOU_OKAY = 1,
        SEND_CONTENT_DOWNLOADED = 2
    }
    public class Functions
    {
        ApplicationDbContext Db = new ApplicationDbContext();

        public List<Boolean> pingDevices(List<Device> devices)
        {
            List<Boolean> statusList = new List<Boolean>();
            Ping ping = new Ping();
            var func = new Functions();
           

            foreach (Device device in devices)
            {
                try
                {
                    var result = func.sendCommand(deviceCommands.SEND_ARE_YOU_OKAY, device.Ip);
                    statusList.Add(result);

                } catch(PingException)
                {
                    statusList.Add(false);
                }
            }
            return statusList;
        }

        public Boolean sendCommand(deviceCommands command, String ip)
        {
            Device device = Db.Devices.Where(d=>d.Ip==ip).FirstOrDefault<Device>();
            string serverIP = device.Ip;
            int serverPort = device.Port;
            bool commandSucceed = false;
            int timeout = 1000;
            try
            {
                var tcpClient = new TcpClient();
                if (tcpClient.ConnectAsync(serverIP, serverPort).Wait(timeout))
                {
            var newStream = tcpClient.GetStream();
            switch (command)
            {
                case deviceCommands.SEND_ARE_YOU_OKAY:
                    try
                    {
                                var bytesToSend = ASCIIEncoding.ASCII.GetBytes("SEND_ARE_YOU_OKAY");
                        newStream.Write(bytesToSend, 0, bytesToSend.Length);
                    }
                    catch { }

                    Byte[] bytes = new Byte[256];
                    String response = null;

                        NetworkStream stream = tcpClient.GetStream();

                            // Translate data bytes to a ASCII string.
                            byte[] bytesToRead = new byte[tcpClient.ReceiveBufferSize];
                            int bytesRead = newStream.Read(bytesToRead, 0, tcpClient.ReceiveBufferSize);
                            response = System.Text.Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);
                            Console.WriteLine("Received: {0}", response);
                            // Process the data sent by the client.
                            response = response.ToUpper();
                            if (response.Equals("STATUS_DEVICE_OK") || response.Equals("STATUS_SERVER_OK")) commandSucceed = true;
                            else break;
                    break;
                case deviceCommands.SEND_NEW_CONTENT:
                    try
                    {
                                var bytesToSend = ASCIIEncoding.ASCII.GetBytes("SEND_NEW_CONTENT");
                        newStream.Write(bytesToSend, 0, bytesToSend.Length);
                    }
                    catch { }

                    bytes = new Byte[256];
                    response = null;

                            stream = tcpClient.GetStream();

                            // Translate data bytes to a ASCII string.
                            bytesToRead = new byte[tcpClient.ReceiveBufferSize];
                            bytesRead = newStream.Read(bytesToRead, 0, tcpClient.ReceiveBufferSize);
                            response = System.Text.Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);
                            Console.WriteLine("Received: {0}", response);
                            // Process the data sent by the client.
                            response = response.ToUpper();
                            if (response.Equals("STATUS_CONTENT_READY_TO_DOWNLOAD")) commandSucceed = true;
                            else break;
                    break;
                case deviceCommands.SEND_CONTENT_DOWNLOADED:
                    try
                    {
                                var bytesToSend = ASCIIEncoding.ASCII.GetBytes("SEND_CONTENT_DOWNLOADED");
                        newStream.Write(bytesToSend, 0, bytesToSend.Length);
                    }
                    catch { }

                    bytes = new Byte[256];
                    response = null;

                            stream = tcpClient.GetStream();

                            // Translate data bytes to a ASCII string.
                            bytesToRead = new byte[tcpClient.ReceiveBufferSize];
                            bytesRead = newStream.Read(bytesToRead, 0, tcpClient.ReceiveBufferSize);
                            response = System.Text.Encoding.ASCII.GetString(bytesToRead, 0, bytesRead);
                            Console.WriteLine("Received: {0}", response);
                            // Process the data sent by the client.
                            response = response.ToUpper();
                            if (response.Equals("STATUS_CONTENT_DOWNLOADED")) commandSucceed = true;
                            else break;
                            break;
                        }
                        tcpClient.Close();

                }
                else {
                    commandSucceed = false;
                    }
            }
            catch { }
            //tcpListener.Stop();
            return commandSucceed;
        }


    }
}