﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(wwwContentManager.Startup))]
namespace wwwContentManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
